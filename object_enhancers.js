/**
 * Created by ksheppard on 3/11/2015.
 */

var ksObject = function(o) {
  return {
      id: function() {
        return Object.keys(o)[0];
      },
      label: function() {
          return Object.keys(o)[1];
      }
  }
};

var ksArray = function(array) {
    return {
        indexOf: function(object) {
            var index = -1 ;
            var field = new ksObject(object).id();
             for(var i=0 ; i <array.length ; i++) {
                if(array[i][field] == object[field]) {
                    index = i;
                    break;
                }
            }
            return index ;
        }
    }
};